#ifdef DOXYGEN_SHOULD_SKIP_THIS
/**
   Tutorial for obtaining a Epsilon Grasp Quality from graspit API

   Modified from Jennifer Buehler's Graspit API

   Copyright (C) 2016 Pablo Frank

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
#endif   // DOXYGEN_SHOULD_SKIP_THIS

///PFB
#define GRASPITDBG
#include <graspit/debug.h>

#include <grasp_planning_graspit/GraspItSceneManagerHeadless.h>
#include <grasp_planning_graspit/EigenGraspPlanner.h>
#include <grasp_planning_graspit/EigenGraspResult.h>
#include <string>
#include <vector>
///PFB for Qeps
#include <grasp_planning_graspit/GraspQualityEvaluator.h>



/**
 * \brief Tutorial on how to load a world and run the Eigengrasp planner.
 */
int main(int argc, char **argv)
{
  if (argc < 3)
  {
    std::cerr << "Usage: " << argv[0] << " <world-filename> <output-directory>" << std::endl;
    return 1;
  }

  std::string worldFilename = argv[1];
  std::string outputDirectory = argv[2];

  if (worldFilename.empty())
  {
    std::cerr << "You have to specify a world" << std::endl;
    return 1;
  }


  // Create the graspit world manager.
  SHARED_PTR<GraspIt::GraspItSceneManager> graspitMgr(new GraspIt::GraspItSceneManagerHeadless());

  // Load the graspit world
  graspitMgr->loadWorld(worldFilename);

  std::string name = "QualityEvaluator1";
  //Create the graspit accessor
  SHARED_PTR<GraspIt::GraspQualityEvaluator > graspitQev (new GraspIt::GraspQualityEvaluator(name, graspitMgr) );

  //! numHyperPlanes x 7 matrix of plane coefficients---> representing the resulting GWS
  double **hyperPlanes;
  //! number of 6D hyperplanes bounding this GWS
  int numHyperPlanes = 0;

  // Default call has last three params default to true
  // double qEps = graspitQev->computeGWSandQuality(hyperplanes, &numHyperPlanes);
  double qEps = graspitQev->computeGWSandQuality(hyperPlanes, &numHyperPlanes,true, true, true);

  // uncomment for multiple tests
  //    for (int ii = 0; ii < 50; ii++)
  //    {
  //      graspitQev->computeGWSandQuality();
  //    }

  ///NOTE: the wrenches themselves are printed inside gws.cpp in int L1GWS::build(/**/)
//  DBGP(" x: " << grasp->getContact(i)->getMate()->wrench[w].force.x() <<
//       " y: " << grasp->getContact(i)->getMate()->wrench[w].force.y() <<
//       " z: " << grasp->getContact(i)->getMate()->wrench[w].force.z() );
//  DBGP("tx: " << grasp->getContact(i)->getMate()->wrench[w].torque.x() <<
//       " ty: " << grasp->getContact(i)->getMate()->wrench[w].torque.y() <<
//       " tz: " << grasp->getContact(i)->getMate()->wrench[w].torque.z() );

  std::cout<< "\n======================================= " <<std::endl;
  std::cout<< "resulting Epsilon Grasp Quality: "<< qEps <<std::endl;
  std::cout<< "=======================================\n\n " <<std::endl;

  ///TODO: following doesn't quite work
  /// might need to use vector<vector<int> > &hyperPlanes instead
  /// and fill it inside GraspQualityEvaluayor::computeGWSandQuality
  // Printing the hyperplanes
  //-------------------------
//  for(int ii = 0; ii< numHyperPlanes; ii++)
//  {
//    std::ostringstream oss;
//    oss << ii <<": ";
//    //double hp[7];
//    for(int jj = 0; jj< 7; jj++)
//    {
//      oss << hyperPlanes[ii][jj] << ", ";
//      //hp[jj] = hyperPlanes[jj][ii];
//    }

//    std::cout<< std::setw(10) << oss.str()  << std::endl;
//  }

  return 0;
}
